import java.util.Scanner;
import java.util.Arrays;

public class Solution {
    public int[] productExceptSelf(int[] nums) {
        int n = nums.length;
        
        // Initialize arrays to store products from left and right sides
        int[] leftProducts = new int[n];
        int[] rightProducts = new int[n];
        
        // Initialize the result array
        int[] result = new int[n];
        
        // Calculate the products of the prefixes
        leftProducts[0] = 1;
        for (int i = 1; i < n; i++) {
            leftProducts[i] = leftProducts[i - 1] * nums[i - 1];
        }
        
        // Calculate the products of the suffixes
        rightProducts[n - 1] = 1;
        for (int i = n - 2; i >= 0; i--) {
            rightProducts[i] = rightProducts[i + 1] * nums[i + 1];
        }
        
        // Calculate the result array
        for (int i = 0; i < n; i++) {
            result[i] = leftProducts[i] * rightProducts[i];
        }
        
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Solution solution = new Solution();
        
        System.out.print("Enter the length of the array: ");
        int length = scanner.nextInt();
        
        int[] nums = new int[length];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < length; i++) {
            nums[i] = scanner.nextInt();
        }
        
        System.out.println("Output: " + Arrays.toString(solution.productExceptSelf(nums)));
        
        scanner.close();
    }
}
